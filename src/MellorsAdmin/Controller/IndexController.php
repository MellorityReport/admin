<?php
/**
 * OBADJA - Frontend Application for yocondo.de (http://de.yocondo.de/)
 * 
 * @link      http://github.com/mellors/obadja for the canonical source repository
 * @author    Marcel Mellor <info@mellors.de>
 * @copyright Copyright (c) 2012 Mellors (http://www.mellors.de)
 */

namespace MellorsAdmin\Controller;

use MellorsApp\Controller\ViewConfigAware;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

 
class IndexController extends AbstractActionController implements ViewConfigAware
{    
    protected $_viewModel;
    
    /**
     * action for user grid
     *
     * @return ViewModel
     */

    
    public function indexAction(){
        $entityName = $this->params('entity_alias');
        $config = $this->getServiceLocator()->get('config');
        $grid = false;
        
        if(isset($config['admin']) && is_array($config['admin'])){
            if(isset($config['admin'][$entityName])){
                $entityClass = $config['admin'][$entityName];
                $grid = $this->getAdminGrid($entityClass);
            }
        }
        
        return $grid;
    }

    public function subgridAction(){
      $fieldName = $this->params()->fromQuery('fieldname');
      echo $fieldName;
      $grid = $this->getAdminGrid($this->params()->fromQuery('_entity'))['grid'];

      $result = new ViewModel(array(
          'grid' => json_encode($grid->prepareGridData($this->getRequest(), array('fieldName' => $fieldName)))
      ));
      $result->setTerminal(true);
      $result->setTemplate('mellors-admin/index/ajax.phtml');
      return $result;
    }
    
    public function uploadAction()
    {
        
        $request = $this->getRequest();
        $files   = $request->getFiles();
        $basePath =  $this->getServiceLocator()->get('viewhelpermanager')->get('basePath');
		

        //Save origin image
        $filter = new \Zend\Filter\File\RenameUpload(realpath("./public/media/"));
        
        //$filter->setUseUploadName(true);
        $filter->setOverwrite(true);
        $filter->setUseUploadExtension(true);
        $filter->setRandomize(true);
        $upload = $filter->filter($files['upload']);
        
        //Save thumbnail
        $width   = $this->params('width', 30);
        $height  = $this->params('height', 30);
        $imagine = $this->getServiceLocator()->get('Imagine');
        $filePath = $upload['tmp_name'];
        
        $image = $imagine->open($filePath);
        $image->resize($image->getSize()->widen(200))
            ->save(str_replace('media', 'media/thumbnails', $filePath));


        $result = new ViewModel(array(
              'filePath' => $basePath('media/' . basename($filePath)),
              'callback' => $this->params()->fromQuery('CKEditorFuncNum')
        ));
        
        $result->setTerminal(true);
        return $result;
    }
    
    public function imagelistAction()
    {
 
        $images = array();
        $basePath =  $this->getServiceLocator()->get('viewhelpermanager')->get('basePath');

        if ($handle = opendir("./public/media/")) {
            $i = 0;
            while (false !== ($file = readdir($handle))) {
                $i++;
                $images[] = array(
                    "image" => $basePath('media/' . $file),
                    "thumb" => $basePath('media/thumbnails/' . $file),
                );
            }
            closedir($handle);
        }
 
        $result = new ViewModel(array(
              'images' => $images
         ));
        
        $result->setTerminal(true);
        return $result;
    }
    
    
    /**
     * create jqgrid instance and set some default / specific options
     *
     * @param string $entity
     * @param array $visibleColumns
     * @param array $hiddenColumns
     * @return ViewModel
     */
    
    protected function getAdminGrid($entity, $visibleColumns = array(), $hiddenColumns = array())
    {
        $grid = $this->getServiceLocator()->get('jqgrid')->setGridIdentity($entity, 'admin');
        return array('grid' => $grid);
    }
    
}