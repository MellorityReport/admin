<?php
/**
 * OBADJA - Frontend Application for yocondo.de (http://de.yocondo.de/)
 * 
 * @link      http://github.com/mellors/obadja for the canonical source repository
 * @author    Marcel Mellor <info@mellors.de>
 * @copyright Copyright (c) 2012 Mellors (http://www.mellors.de)
 */


namespace MellorsAdmin;

//JqGrid options
$beforeShowForm = new \Zend\Json\Expr("function(){ require(['jj'], function (jj) {
    setTimeout(function(){
        $('#tr_body').
            show().
            find('#body').
            ckeditor( function() {}, { 
                extraPlugins: 'pbckcode',
                allowedContent : true,
                forcePasteAsPlainText : true,
                entities : false,
                toolbar :[
                    { name: 'workflow', items: ['Undo', 'Redo'] },
                    { name: 'basicstyles', items : [ 'Bold','Italic' ] },
                    { name: 'format', items: ['Format']},
                    { name: 'paragraph', items : [ 'NumberedList','BulletedList', 'Link', 'pbckcode', 'Source'] },
                    { name: 'tools', items : [ 'Image' ] }
                ],
                pbckcode : {
                    theme : 'tomorrow_night',
                    highlighter : 'PRISM'
                },
                filebrowserUploadUrl : jj.get('baseUrl') + '/admin/upload'
            });
            }, 150); 		
});}");

$onClose = new \Zend\Json\Expr("function(){	
    if(typeof CKEDITOR.instances['body'] != 'undefined'){
        CKEDITOR.instances['body'].destroy();
    }    		
}");

$onView = new \Zend\Json\Expr("function(form){
    var callback = function(){
        $('.ui-icon-closethick').trigger('click');
    }
    showPreview(form.find('#v_id span:last').text(), callback);
}");
            
$afterSubmit = new \Zend\Json\Expr("function(response, postdata){ 
    var result = jQuery.parseJSON(response.responseText);
    if(response.status == 200){ 
        showPreview(postdata.id, function(){});
        $('.topinfo').addClass('ui-state-highlight').html('Entry saved'); 
        var tinfoel = $('.tinfo').show();
        tinfoel.delay(3000).fadeOut();
        return [true,''];
    } else {
        return [result.success, result.message, result.id];
    }
}");


return array(
    'navigation' => array(
        'admin_navigation' => array()
    ),
    'service_manager' => array(
        'invokables' => array(
            'Imagine' => 'Imagine\Gd\Imagine'
        ),
        'factories' => array( 
            'admin_navigation' => 'MellorsAdmin\Navigation\Service\AdminNavigationFactory',
        )
    ),  
    'router' => array(
        'routes' => array(            
            'admin' => array(
                'type'    => 'Literal',
                'priority' => 1000,                
                'options' => array(
                    'route'    => '/admin',
                    'defaults' => array(
                        '__NAMESPACE__' => 'MellorsAdmin\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                        'module'        => 'MellorsAdmin'
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'entity' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '[/:entity_alias]',
                            'constraints' => array(
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                        ),
                    ),
                    'subgrid' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '[/subgrid]',
                             'defaults' => array(
                                'action'        => 'subgrid',
                             )
                        ),

                    ),
                    'upload' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '[/upload]',
                            'constraints' => array(
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                             'defaults' => array(
                                'action'        => 'upload',
                             )
                        ),
                    )   
                )
            )
           
        ),
    ),

    'zfc_rbac' => array(
        'guards' => [
            'ZfcRbac\Guard\RouteGuard' => [
                'admin*' => ['admin']
            ]
        ],
        'permission_providers' => [
            'ZfcRbac\Permission\InMemoryPermissionProvider' => [
                'admin' => ['admin']
            ]
        ]
    ),
    'controllers' => array(
        'invokables' => array(
            'MellorsAdmin\Controller\Index' => 'MellorsAdmin\Controller\IndexController'
        ),
    ),
    'view_manager' => array(
        'doctype'                  => 'HTML5',
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    
    'jqgrid' => array(
        'grid_options' => array(
            'mtype' => 'GET',
            'cmTemplate' => new \Zend\Json\Expr("{
                
            }"),
            'cellEdit' => false,
            'rowActionButtons' => array(array('name' => 'view', 'icon' => 'document'))
            /*
            'subGridBeforeExpand' => new \Zend\Json\Expr("function (subgrid_id, row_id) {
                $.jgrid.parentRowId = row_id;
            }")
             * 
             */
        ),

        'edit_parameters' =>  array(
            'mtype' => 'PUT',
            'beforeShowForm' => $beforeShowForm,
            'onClose' => $onClose,
            'afterSubmit' => $afterSubmit,
        ),
        'add_parameters' =>     array(
            'mtype' => 'POST',
            'beforeShowForm' => $beforeShowForm,
            'onClose' => $onClose,
            /*
            'beforeSubmit' => new \Zend\Json\Expr("function(postdata, id){
                if(!postdata.question){
                    postdata.question = $.jgrid.parentRowId;
                }
                alert('TEST!');
                return [true, 'Allet jut']; 
            }")
             * 
             */
         ),
        'view_parameters' => array(
            'beforeShowForm' => $onView,
            'mtype' => 'GET',
        ),
        'delete_parameters' => array(
            'mtype' =>  'DELETE'
        ),

        'first_data_as_local'               => false,
        'grid_url_generator'  => function ($sm, $entity, $fieldName, $targetEntity, $urlType) {
            echo 'test';
           switch($urlType){
                case  \SynergyDataGrid\Grid\GridType\BaseGrid::DYNAMIC_URL_TYPE_ROW_EXPAND:
                    //@var $helper \Zend\View\Helper\Url
                    //Get key
                    $reflect = new \ReflectionClass(new $targetEntity());
                    $helper = $sm->get('viewhelpermanager')->get('url');
                    $url    = $helper('admin/subgrid',
                             array(
                                     //'entity_alias' => $targetEntity,
                                     //'fieldName' => $fieldName
                             )
                     );
                return new \Zend\Json\Expr("'$url?fieldname=" . $fieldName . "&subgridid='+row_id");
            }
        },

        'column_model' => array(
            'active' => array('width' => 13),
            'title'     => array('hidden' => false),
            'teaser'    => array('hidden' => false),
            'id' => array('hidden' => true),
            'start' => array('editoptions' => array(
                'dataInit' => new \Zend\Json\Expr('function(el){
                    $(el).datepicker();
                }')
            )),
            'end' => array('editoptions' => array(
                'dataInit' => new \Zend\Json\Expr('function(el){
                    $(el).datepicker();
                }')
            )),
        )        
    )
);
