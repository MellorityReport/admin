<?php
/**
 * OBADJA - Frontend Application for yocondo.de (http://de.yocondo.de/)
 * 
 * @link      http://github.com/mellors/obadja for the canonical source repository
 * @author    Marcel Mellor <info@mellors.de>
 * @copyright Copyright (c) 2012 Mellors (http://www.mellors.de)
 */

namespace MellorsAdmin;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module extends \MellorsApp\AbstractModule
{
    
    public function getDir() 
    {
        return __DIR__;
    }
    
    public function getNamespace() 
    {
        return __NAMESPACE__;
    }
    
    public function onBootstrap(MvcEvent $event) {
        
        $config = $event->getApplication()->getServiceManager()->get('config');
        $renderer = $event->getApplication()->getServiceManager()->get('Zend\View\Renderer\PhpRenderer');             

        if(!isset($config['admin']) || !is_array($config['admin'])){
            parent::onBootstrap($event);
        }
        
        foreach($config['admin'] as $name => $entity){
            $page = \Zend\Navigation\Page\AbstractPage::factory(array(
                      'label' => ucfirst($name),
                      'route' => 'admin/entity',
                      'params' => array('entity_alias' => $name),
                      'permission' => 'admin',
                      'group' => 'admin-link',
                      'class' => 'admin-link sites ui-state-default ui-corner-top'
            ));
            $page->setRouter($event->getApplication()->getServiceManager()->get('router'))->setActive();
            $renderer->navigation('admin_navigation')->addPage($page);
        }
        
    }
    
}
